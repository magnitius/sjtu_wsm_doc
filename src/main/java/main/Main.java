package main;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import webservlets.ChatServlet;
import webservlets.RequestServlet;
import webservlets.SearchPageServlet;

/**
 * Created by antonvoronov on 15.05.17.
 */
public class Main {
    public static void main(String[] args) throws Exception{
        Server server = new Server(8080);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        RequestServlet requestServlet = new RequestServlet();
//        SearchPageServlet searchPageServlet = new SearchPageServlet();



        context.addServlet(new ServletHolder(requestServlet), "/search");
        context.addServlet(new ServletHolder(new ChatServlet()), "/chat");
        //context.addServlet(new ServletHolder(searchPageServlet), "/search");
        ResourceHandler resource_handler = new ResourceHandler();
        resource_handler.setDirectoriesListed(true);
        resource_handler.setResourceBase("webpages");

        HandlerList handlers = new HandlerList();
        handlers.setHandlers(new Handler[]{resource_handler, context});
        server.setHandler(handlers);

        server.start();
        server.join();

    }
}
