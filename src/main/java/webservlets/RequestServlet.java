package webservlets;

import templater.PageGenerator;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Created by antonvoronov on 15.05.17.
 */
public class RequestServlet extends HttpServlet{

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, Object> pageVariables = PageGenerator.createPageVariablesMap(req);
        //pageVariables.put("text","");
        resp.getWriter().println(PageGenerator.instance().getPage("index.html", pageVariables));
        resp.setContentType("text/html;charset=utf-8");
        resp.setStatus(HttpServletResponse.SC_OK);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, Object> pageVariables = PageGenerator.createPageVariablesMap(req);
        System.out.println("Dedug: do post");
        String message = req.getParameter("text");
        resp.setContentType("text/html;charset=utf-8");
        System.out.println("User search for: " + message);
//
//        if (message == null || message.isEmpty()) {
//            resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
//        } else {
//
//            resp.setStatus(HttpServletResponse.SC_OK);
//        }
        //pageVariables.put("text", message == null ? "" : message);
        // call setting post
        Result[] results = {new Result("Yandex", "https://yandex.ru/", "Yandex - find whhat you need!"),
                new Result("Yandex", "https://yandex.ru/", "Yandex - find what you need!"),
                new Result("Yandex", "https://yandex.ru/", "Yandex - find what you need!"),
                new Result("Yandex", "https://yandex.ru/", "Yandex - find what you need!")};
        pageVariables.put("results", results);
        resp.getWriter().println(PageGenerator.instance().getPage("search.html", pageVariables));
    }


//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//
//    }
}
