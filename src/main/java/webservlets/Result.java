package webservlets;

/**
 * Created by antonvoronov on 23.05.17.
 */
public class Result {

    private String tittle;
    private String link;
    private String content;

    public Result(String tittle, String link, String content) {
        this.tittle = tittle;
        this.link = link;
        this.content = content;
    }

    public Result() {
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
