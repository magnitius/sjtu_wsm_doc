package webservlets;

import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import templater.PageGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Created by antonvoronov on 23.05.17.
 */
public class SearchPageServlet extends HttpServlet{
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String message = req.getParameter("text");
        System.out.println("User search for: " + message);
        Map<String, Object> pageVariables = PageGenerator.createPageVariablesMap(req);
        Result[] results = {new Result("Yandex", "https://yandex.ru/", "Yandex - find whhat you need!"),
                new Result("Yandex", "https://yandex.ru/", "Yandex - find what you need!"),
                new Result("Yandex", "https://yandex.ru/", "Yandex - find what you need!"),
                new Result("Yandex", "https://yandex.ru/", "Yandex - find what you need!")};
        pageVariables.put("results", results);
        resp.getWriter().println(PageGenerator.instance().getPage("search.html", pageVariables));
        resp.setContentType("text/html;charset=utf-8");
    }


}
